# University of Pennsylvania Sample Project #

Using [Bootstrap](http://getbootstrap.com/), [jQuery](https://jquery.com/) and any additonal javascript libraries of your choice, output the data provided in `data\images.json` and follow the instructions below

## Intructions ##
1. Fork the Sample Project repository
    * Check the Inherit repository user/group permissions
2. Using the provided Framework display the following inside the container div
    * Responsive 2 x 3 grid of standard resolution images
    * Caption
    * Formated date (i.e. October 1st 2015)
3. Commit your changes and provide us with the URL of your repo
